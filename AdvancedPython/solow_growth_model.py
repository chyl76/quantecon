# Author: Christopher Lo
# Date: 19th August 2018
# Quantative Economics https://lectures.quantecon.org/
# Section: OOP II: Building Classes
# Example: Solow Growth Model

class Solow:
    """
    Implements the Solow growth model with update rule

        k_{t+1} = [(s z k^α_t) + (1 - δ)k_t] /(1 + n)

    """
    def __init__(self, n, s, sig, alpha, z, k):
        """
        n (float): population growth rate
        s (float): savings rate
        sig (float): depreciation rate
        alpha (float): share of labor
        z (float): productivity
        k (float): current capital stock
        """
        self.n, self.s, self.sig, self.alpha, self.z = n, s, sig, alpha, z
        self.k = k

    def

if __name__ == "__main__":
